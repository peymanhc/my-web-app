## Available Scripts

In the project directory, you can run:

### `cd/FakeServer json-server --watch db.json`
The server runs <br />
[http://localhost:3000](http://localhost:3000)

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:2000](http://localhost:2000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
