import React from 'react'
import styles from './DetailComment.module.css'
const DetailComment = () => {
    return (
        <div className="col-md-3" >
            <div className="m-5">
                <ul className={styles.c_comments_user_shopping}>
                    <li>
                        <div className={styles.cell_name}>
                            Peymanhc
                    </div>
                    </li>
                    <li>
                        <div className={styles.cell}>
                            5 may 2020
                    </div>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default DetailComment
