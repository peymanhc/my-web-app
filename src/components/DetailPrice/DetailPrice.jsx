import React from 'react'
import style from './DetailPrice.module.css'
const DetailPrice = (props) => {
    return (
    <div className={style.price}>${props.children}</div>
    )
}

export default DetailPrice
