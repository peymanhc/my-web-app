import React from 'react'
import styles from './Specifications.module.css'

const Specifications = () => {
    return (
        <div className={styles.Specifications} >
            <div className="column" style={{ width: "100%" }} >
                <div className={styles.table_container}>
                    <table className={styles.specifications_table}>
                        <tbody>
                            <tr>
                                <th colSpan="2">Weights</th>
                            </tr>
                            <tr>
                                <td>Specifications 1 (lbs)</td>
                                <td>4,400</td>
                            </tr>
                            <tr>
                                <td>Specifications 2 (lbs)</td>
                                <td>605</td>
                            </tr>
                            <tr>
                                <td>Specifications 3 (lbs)</td>
                                <td>5,950</td>
                            </tr>
                            <tr>
                                <td className="last">Specifications 4 (lbs)</td>
                                <td>1,550</td>
                            </tr>
                            <tr>
                                <th colSpan="2">Measurements</th>
                            </tr>
                            <tr>
                                <td>Specifications 1</td>
                                <td>23' 11"</td>
                            </tr>
                            <tr>
                                <td>Specifications 2</td>
                                <td>96"</td>
                            </tr>
                            <tr>
                                <td>Specifications 3</td>
                                <td>128"</td>
                            </tr>
                            <tr>
                                <td>Specifications 4</td>
                                <td>78"</td>
                            </tr>
                            <tr>
                                <td>Specifications 5</td>
                                <td>60 x 74</td>
                            </tr>
                            <tr>
                                <td>Specifications 6</td>
                                <td>42 x 64</td>
                            </tr>
                            <tr>
                                <td className="last">Specifications 7</td>
                                <td>188"</td>
                            </tr>
                            <tr>
                                <th colSpan="2">Comfortable </th>
                            </tr>
                            <tr>
                                <td>Specifications 1</td>
                                <td>48</td>
                            </tr>
                            <tr>
                                <td>Specifications 2 (gals)</td>
                                <td>30.5</td>
                            </tr>
                            <tr>
                                <td className="last">Specifications 3 (gals)</td>
                                <td>30.5</td>
                            </tr>
                            <tr>
                                <th colSpan="2">Other</th>
                            </tr>
                            <tr>
                                <td className="last">Specifications 1</td>
                                <td>2-4</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Specifications
