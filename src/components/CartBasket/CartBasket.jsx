import React from 'react'
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import styles from './CartBasket.module.css'


const CartBasket = (props) => {
    
    const addPrice = () => {
        props.addToCart(props.orders + props.ANARPRICE)
        props.incrementbtn(props.count )
    };
    const removePrice = () => {
        props.addToCart(props.orders - props.ANARPRICE)
        props.decrementbtn(props.count )
    };

    return (
        <td>
            <div className={styles.button_container}>
                <a href="#+" onClick={addPrice} className={styles.cart_qty_btn} type="button">+</a>
                <input type="text" className="qty form-control" onChange={addPrice} value={props.count} />
                <a href="#-" onClick={removePrice} className={styles.cart_qty_btn} type="button">-</a>

            </div>
        </td>
    )
}

const mapStateToProps = state => {
    return {
        orders: state.cart.orders,
        count: state.cart.counter,
        ANARPRICE: state.cart.abanar
    };
};

const mapDispatchToProps = dispatch => {

    return {
        addToCart: (orders) => dispatch(actions.addToCart(orders)),
        incrementbtn: (count) => dispatch(actions.incrementbtn(count)),
        decrementbtn: (count) => dispatch(actions.decrementbtn(count)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CartBasket)
