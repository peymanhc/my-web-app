import React from 'react';
import style from './Detailbtn.module.css'
function Detailbtn(props) {

    return (
        <button 
        onClick={props.added}
        className={style.btn}>Add to cart<span className={style.bg}></span></button>
    );
}

export default Detailbtn;