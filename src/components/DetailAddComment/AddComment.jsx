import React from 'react'
import styles from './AddComment.module.css'
const AddComment = () => {
    return (
        <div className="mt-5" >
            <form className="card w-100 ">
                <div className="card-body">
                    <h2>Add Your Comment</h2>
                    <div className="form-group">
                        <input type="text" id="post-text" className="form-control" placeholder="Your Name" />
                    </div>
                    <div className="form-group">
                        <input type="text" id="post-user" className="form-control" placeholder="Your Comment" />
                    </div>
                    <button className={styles.slide}> Add Comment</button>
                </div>
            </form>
        </div>
    )
}

export default AddComment
