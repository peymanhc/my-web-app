import React from 'react'
import './CardImg.module.css'
const Cardimg = (props) => {
    return (
        <figure>
            <img alt={props.Alt} src={props.ImgSrc} />
        </figure>
    )
}

export default Cardimg
