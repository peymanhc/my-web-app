import React from 'react'
import 'font-awesome/css/font-awesome.min.css';

const TopBuy = () => {
    return (
        <div className="top">
            <i className="fa fa-cart-plus carticon" aria-hidden="true"></i>
        </div>
    )
}

export default TopBuy
