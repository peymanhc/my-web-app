import React from 'react';
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import './AddBuy.css'
import TopBuy from '../TopBuySec/TopBuy';
import BtnBuy from '../BtnBuy/BtnBuy';
import InSideBuy from '../InSideBuy/InSideBuy';

function Addbuy(props) {
    return (
        <div className="wrapper">
            <p className="countCart" >{props.count}</p>
            <div className="container">
                <TopBuy />
                <BtnBuy price={props.price} />
            </div>
            <div style={{ minHeight: "10em", paddingBottom: "12em" }}>
                <InSideBuy price={props.price} count={props.count} />
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        count: state.cart.counter
    };
};

const mapDispatchToProps = dispatch => {

    return {
        incrementbtn: (count) => dispatch(actions.incrementbtn(count)),
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Addbuy);