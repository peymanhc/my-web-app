import React from 'react'

const CartPrice = (props) => {
    return (
        <td>
            <input style={{lineHeight:"0.5em"}} type="text" value={props.price} className="price form-control" disabled />
        </td>
    )
}

export default CartPrice
