import React from 'react';
import './Header.css'
import { Items } from '../Header-item/Items';
import { Link } from 'react-router-dom'
import { Dropdown, Button, ButtonGroup } from 'react-bootstrap'
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

function Header({ logOut }) {    
    return (
        <header>
            <nav className="navbar navbar-expand-lg">
                <li className="mt-1  list-inline-item">
                    <a href="/" className="btnlogo btn btn-outline-white btn-rounded">Logo</a>
                </li>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon">
                        <div className="burgericon" ></div>
                        <div className="burgericon mt-2" ></div>
                        <div className="burgericon mt-2" ></div>
                    </span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="col-md-6 navbar-nav mr-auto">
                        <Items />
                    </ul>
                    <li className="mt-1  list-inline-item">
                        {
                            localStorage.getItem("email") !== null
                                ? <Dropdown as={ButtonGroup}>
                                    <Button className="btnsignup rounded-0 " style={{ lineHeight: "10px" }} >Peymanhc</Button>
                                    <Dropdown.Toggle className="btnsignup btn btn-outline-white rounded-0 p-2" split id="dropdown-split-basic" />
                                    <Dropdown.Menu>
                                        <Dropdown.Item href="MyProfile">MyProfile</Dropdown.Item>
                                        <Dropdown.Item href="MyCart">My Basket</Dropdown.Item>
                                        <Dropdown.Divider />
                                        <Dropdown.Item onClick={logOut} >Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                : <Link to="Login_Register" className="btnsignup btn btn-outline-white btn-rounded">Login / Register</Link>
                        }
                    </li>
                </div>
            </nav>
        </header>
    );
}

const mapStateToProps = (state) => {
    return {
        logOutData: state.auth.data,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logOut: logOut => (dispatch(actions.logout(logOut))),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);