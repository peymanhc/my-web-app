import React from 'react'

const BtnBuy = (props) => {

    return (
        <div>
            <h1>Buy</h1>
            <p>${props.price}</p>
        </div>
    )
}

export default BtnBuy
