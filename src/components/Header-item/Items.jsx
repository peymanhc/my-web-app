import React from 'react'
import Item from './Item'
import Aux from '../../hoc/Aux'

export const Items = () => {
    return (
        <Aux>
            <Item exact LinkTo="/" >Home</Item>
            <Item LinkTo="About" >About</Item>
            <Item LinkTo="Contacts" >Contacts</Item>
            <Item LinkTo="Blogs" >Blogs</Item>
        </Aux>
    )
}
