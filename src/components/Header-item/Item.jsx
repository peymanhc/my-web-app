import React from 'react'
import {NavLink} from 'react-router-dom'
const Item = (props) => {
  return (
    <li className="nav-item">
      <NavLink className="nav-link" to={props.LinkTo}>{props.children}</NavLink>
    </li>
  )
}
export default Item
