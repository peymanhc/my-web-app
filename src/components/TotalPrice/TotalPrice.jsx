import React from 'react'

const TotalPrice = (props) => {
    return (
        <tfoot>
            <tr>
    <td colSpan="4"></td><td align="right"><strong>TOTAL = $  <span id="total" className="total">{props.price}</span></strong></td>
            </tr>
        </tfoot>
    )
}

export default TotalPrice
