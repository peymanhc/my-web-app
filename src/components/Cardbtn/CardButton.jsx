import React from 'react';
import style from './Cardbtn.module.css'
function CardButton(props) {

    return (
        <button 
        onClick={props.added}
        className={style.btn}>Add to cart<span className={style.bg}></span></button>
    );
}

export default CardButton;