import React from 'react'
import './Pagination.css'
const Pagination = () => {
    return (
        <nav className="row justify-content-center mt-5 " aria-label="Page navigation example">
            <ul className="pagination">
                <li className="page-item">
                    <a className="page-link" href="#perv" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">Previous</span>
                    </a>
                </li>
                <li className="page-item active"><a className="page-link" href="#1">1</a></li>
                <li className="page-item"><a className="page-link" href="#2">2</a></li>
                <li className="page-item"><a className="page-link" href="#3">3</a></li>
                <li className="page-item"><a className="page-link" href="#3">4</a></li>
                <li className="page-item"><a className="page-link" href="#3">5</a></li>
                <li className="page-item">
                    <a className="page-link" href="#next" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span className="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    )
}

export default Pagination
