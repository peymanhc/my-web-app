import React from 'react'
import styles from './search.module.css'
const Search = () => {
    return (
        <div>
            <form className={styles.searchbox}>
                <input className={styles.inputsearch} type="text" placeholder="Search here..." />
            </form>
        </div>
    )
}

export default Search
