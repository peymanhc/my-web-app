import React, { useState } from 'react'
import styles from './Login.module.css'
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

function Login({
    loginData,
    setLoginData,
}) {

    const [email, setemail] = useState('')
    const [password, setpassword] = useState('')

    const LoginPost = (e) => {
        e.preventDefault();
        setLoginData({
            email,
            password,
        });
        window.location='/';
    }
    return (
        <form onSubmit={LoginPost} className="m-3 justify-content-center formLogin" >
            <div className="form-group">
                <label >Email address</label>
                <input
                    value={email}
                    onChange={(e) => setemail(e.target.value)}
                    type="email"
                    className="form-control"
                    placeholder="Enter email" />
            </div>
            <div className="form-group">
                <label >Password</label>
                <input
                    onChange={(e) => setpassword(e.target.value)}
                    value={password}
                    type="password"
                    className="form-control"
                    placeholder="Password" />
            </div>
            <div className="form-group form-check">
                <input
                    type="checkbox"
                    className="form-check-input"
                    id="exampleCheck1" />
                <label className="form-check-label" htmlFor="exampleCheck1">Remember Me</label>
            </div>
            <button type="submit" className={`btn ${styles.btnprimary}`}>Login</button>
        </form>
    )
}

const mapStateToProps = (state) => {
    return {
        loginData: state.auth.data,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setLoginData: data => (dispatch(actions.setLoginInfo(data))),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login)