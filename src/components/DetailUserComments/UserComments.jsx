import React, {useState} from 'react'
import styles from './UserComments.module.css'
import DetailComment from '../DetailComment/DetailComment'
import CommentUser from '../commentUser/CommentUser'
import DetailStarComment from '../DetailCommentStar/DetailStarComment'
import AddComment from '../DetailAddComment/AddComment'

function UserComments() {

    const [showResults, setShowResults] = useState(false)
    const ShowForm = () => setShowResults(true)

    return (
        <div className={styles.UserComments} >
            <DetailStarComment showAddComment={ShowForm} />
            { showResults ? <AddComment /> : null }
            <hr className="mt-5" />
            <div className="row" >
                <DetailComment />
                <CommentUser />
            </div>
            <hr className="mt-5" />
            <div className="row" >
                <DetailComment />
                <CommentUser />
            </div>
            <hr className="mt-5" />
            <div className="row" >
                <DetailComment />
                <CommentUser />
            </div>
        </div>
    )
}

export default UserComments
