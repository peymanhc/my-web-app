import React from 'react'
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
    navcolorbtn: {
        color: "#b10018"
    },
    navtabs: {
        border : "1px solid red"
    },
})

const DetailNav = () => {

    const classes = useStyles();

    return (
        <nav
            style={{height:"auto" }}>
            <div
                className={`nav nav-tabs ${classes.navtabs}`}
                id="nav-tab"
                role="tablist">
                <a

                    className={`nav-item nav-link active ${classes.navcolorbtn}`}
                    id="nav-home-tab"
                    data-toggle="tab"
                    href="#nav-home"
                    role="tab"
                    aria-controls="nav-home"
                    aria-selected="true">Review</a>
                <a
                    className={`nav-item nav-link ${classes.navcolorbtn}`}
                    id="nav-profile-tab"
                    data-toggle="tab"
                    href="#nav-profile"
                    role="tab"
                    aria-controls="nav-profile"
                    aria-selected="false">Specifications</a>
                <a
                    className={`nav-item nav-link ${classes.navcolorbtn}`}
                    id="nav-contact-tab"
                    data-toggle="tab"
                    href="#nav-contact"
                    role="tab"
                    aria-controls="nav-contact"
                    aria-selected="false">User comments</a>
            </div>
        </nav>

    )
}

export default DetailNav
