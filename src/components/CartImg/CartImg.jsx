import React from 'react'
import styles from './CartImg.module.css'
const CartImg = () => {
    return (
        <td>
            <div className={styles.product_img}>
                <div className={styles.img_prdct}>
                    <img alt="Basket" src={require('../../Resources/images/abanar.png')} /></div>
            </div>
        </td>
    )
}

export default CartImg
