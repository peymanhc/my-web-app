import React from 'react'
import styles from './Category.module.css'

const Category = (props) => {
    return (
        <div className={`mt-5 ${styles.toggles}`}>
            <button className={styles.btnCategory} id="showall">Test</button>
            <button className={styles.btnCategory} id="AbAnar">AbAnar</button>
            <button className={styles.btnCategory} id="Peymanhc">Peymanhc</button>
            <button className={styles.btnCategory} id="love">love</button>
            <button className={styles.btnCategory} id="More">More</button>
            <button className={styles.btnCategory} id="Test">Show All</button>
        </div>
    )
}

export default Category
