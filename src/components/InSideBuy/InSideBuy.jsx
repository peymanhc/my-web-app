import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import styles from './InSideBuy.module.css'
import axios from '../../Data Shared/axios'

function InSideBuy(props) {

    const [posts, setposts] = useState([])

    useEffect(() => {
        axios.get('/posts')
            .then(response => {
                const posts = response.data.slice(0, 6);
                setposts(posts)
            })
    }, [])

    return (
        <div className="inside">
            <div className="contents">
                <table className="mt-5">
                    <thead>
                        <tr>
                            <th>Commodity</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    {<tbody>
                        {props.count === 0
                            ? <tr><td>You dont Have any thing in your cart</td></tr>
                            : posts.map((item, i) => (
                                <tr key={item.id} >
                                    <td>{item.title} * {props.count} </td>
                                    <td>${props.price}</td>
                                </tr>
                            ))}
                    </tbody>}
                </table>
            </div>
            <Link to="MyCart" className={styles.btnBuy}>payment</Link>
        </div>
    )
}

export default InSideBuy
