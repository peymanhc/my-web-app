import React from 'react'
import style from './Detailtxt.module.css'
const Cardtxt = (props) => {
    return (
        <div className="row justify-content-center text-center" >
            <div className={style.txt}>{props.children}</div>
        </div>
    )
}

export default Cardtxt
