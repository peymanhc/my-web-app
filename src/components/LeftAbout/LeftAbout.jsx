import React from 'react'

const LeftAbout = () => {
    return (
        <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="aboutus">
                <h2 className="aboutus-title">About Us</h2>
                <p className="aboutus-text">Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
                <a className="aboutus-more" href="#readmore">read more</a>
            </div>
        </div>
    )
}

export default LeftAbout
