import React from 'react'
import 'font-awesome/css/font-awesome.min.css';
const RightAbout = () => {
    return (
        <div className="col-md-5 col-sm-6 col-xs-12">
            <div className="feature">
                <div className="feature-box">
                    <div className="clearfix">
                        <div className="iconset">
                            <span className="fa fa-cog icon"></span>                                        </div>
                        <div className="feature-content">
                            <h4>Work with heart</h4>
                            <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
                        </div>
                    </div>
                </div>
                <div className="feature-box">
                    <div className="clearfix">
                        <div className="iconset">
                            <span className="fa fa-cog icon"></span>                                        </div>
                        <div className="feature-content">
                            <h4>Reliable services</h4>
                            <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to</p>
                        </div>
                    </div>
                </div>
                <div className="feature-box">
                    <div className="clearfix">
                        <div className="iconset">
                            <span className="fa fa-cog icon"></span>
                        </div>
                        <div className="feature-content">
                            <h4>Great support</h4>
                            <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default RightAbout
