import React from 'react'
import styles from './Contact.module.css'

const Contact = () => {
    return (
        <div className={styles.iconCon}>
            <div className={styles.icon} >
                <i className={`fa fa-telegram mt-5 ${styles.iconcircle}`} aria-hidden="true"></i><br/>
                <p className="m-4" >Follow us in Telegram</p>
            </div>
            <div className={styles.icon} >
                <i className={`fa fa-instagram mt-5 ${styles.iconcircle}`} aria-hidden="true"></i><br/>
                <p className="m-4" >Follow us in instagram</p>
            </div>
            <div className={styles.icon} >
                <i className={`fa fa-twitter mt-5 ${styles.iconcircle}`} aria-hidden="true"></i><br/>
                <p className="m-4" >Follow us in twiiter</p>
            </div>
        </div>
    )
}

export default Contact
