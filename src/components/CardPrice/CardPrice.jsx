import React from 'react'
import style from './CardPrice.module.css'
const CardPrice = (props) => {
    return (
        <div className={style.price}>${props.children}</div>
    )
}

export default CardPrice
