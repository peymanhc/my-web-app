import React from 'react'
import style from './CardTitle.module.css'
const CardTitle = (props) => {
    return (
    <div className={style.title}>{props.children}</div>
    )
}

export default CardTitle
