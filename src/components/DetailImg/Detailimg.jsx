import React from 'react'

const Detailimg = (props) => {
    return (
            <img style={{width:"20em", padding:"3em"}} alt={props.Alt} src={props.ImgSrc} />
    )
}

export default Detailimg
