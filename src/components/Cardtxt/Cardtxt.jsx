import React from 'react'
import style from './Cardtxt.module.css'
const Cardtxt = (props) => {
    return (
        <div className={style.desc}>{props.children}</div>
    )
}

export default Cardtxt
