import React from 'react'
import styles from './DetailCommentStart.module.css'

const DetailStarComment = (props) => {
    return (
        <div>
            <div>
                <h3 className={styles.Txt} >Users opinion to :</h3>
                <h5 className={styles.nameCard}><b>AbAnar  </b><small> ( 4 / 5 )</small></h5>
            </div>
            <div className="row mt-4">
                <div className="col-md-6" >
                    <div className="row" >
                        <p className="col-md-2" >Nazar:</p>
                        <div className="col-md-9 mt-2" >
                            <div className={`progress ${styles.progress}`}>
                                <div className={`progress-bar ${styles.progress_bar}`} role="progressbar" style={{ width: "20%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <p className="col-md-2" >Nazar:</p>
                        <div className="col-md-9 mt-2" >
                            <div className={`progress ${styles.progress}`}>
                                <div className={`progress-bar ${styles.progress_bar}`} role="progressbar" style={{ width: "48%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <p className="col-md-2" >Nazar:</p>
                        <div className="col-md-9 mt-2" >
                            <div className={`progress ${styles.progress}`}>
                                <div className={`progress-bar ${styles.progress_bar}`} role="progressbar" style={{ width: "100%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <p className="col-md-2" >Nazar:</p>
                        <div className="col-md-9 mt-2" >
                            <div className={`progress ${styles.progress}`}>
                                <div className={`progress-bar ${styles.progress_bar}`} role="progressbar" style={{ width: "20%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <p className="col-md-2" >Nazar:</p>
                        <div className="col-md-9 mt-2" >
                            <div className={`progress ${styles.progress}`}>
                                <div className={`progress-bar ${styles.progress_bar}`} role="progressbar" style={{ width: "70%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6" >
                    <span className={styles.TitleRate} >Your Rate to this Card</span>
                    <p className={styles.comments_summary}>web design, typography, It's also called placeholder (or filler) text. It's a convenient tool for mock-ups.</p>
                    <button onClick={props.showAddComment} className={styles.slide}> Add Comment</button>
                    <div className="small" >add Comment for a free</div>
                </div>
            </div>
        </div>
    )
}

export default DetailStarComment
