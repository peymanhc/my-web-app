import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'

// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import style from './card.module.css'
import CardTitle from '../Cardtitle/CardTitle';
import Cardtxt from '../Cardtxt/Cardtxt';
import Cardimg from '../Cardimg/Cardimg';
import CardPrice from '../CardPrice/CardPrice';
import CardButton from '../Cardbtn/CardButton';
import axios from '../../Data Shared/axios'
function Card(props) {

    useEffect(() => {
        axios.get('/posts')
    }, [])

    const addPrice = () => {
        props.addToCart(props.orders + props.price)
        props.incrementbtn(props.count)
    };

    return (
        <div className={style.shopcard}>
            <div className="col-xs-4" >
                <Link className="text-decoration-none" to="/detail" >
                    <CardTitle>{props.title}</CardTitle>
                    <Cardtxt>{props.description}</Cardtxt>
                    <Cardimg Alt="alt" ImgSrc={props.image} />
                </Link>
                <div className={style.cta}>
                    <CardPrice >{props.price}</CardPrice>
                    <CardButton added={addPrice} />
                </div>
            </div>
        </div>
    );
}


const mapStateToProps = state => {
    return {
        orders: state.cart.orders,
        count: state.cart.counter,
    };
};

const mapDispatchToProps = dispatch => {

    return {
        addToCart: (orders) => dispatch(actions.addToCart(orders)),
        incrementbtn: (count) => dispatch(actions.incrementbtn(count)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Card);