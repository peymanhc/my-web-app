import React from 'react';
import {Link} from 'react-router-dom'

import './Footer.css'
function Footer(props) {
    return (
        <footer className="f1-color page-footer font-small cyan darken-3">
            <div className="container">
                <ul className="list-unstyled list-inline text-center py-2">

                    <li className="mt-4 mb-2  list-inline-item">
                        <Link to="Login_Register" className="btnsignup btn btn-outline-white btn-rounded">Login / Register for a free</Link>
                    </li>
                </ul>
            </div>
            <div className="footer-copyright text-center py-3">© 2020 Copyright:
                <a href="https://Peymanhc@gmail.com/"> Peymanhc@gmail.com</a>
            </div>
        </footer>

    );
}

export default Footer;