import React, { useState } from 'react'
import styles from './Register.module.css'
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

function Register({setregisterdata}) {

    const [email, setemail] = useState('')
    const [name, setname] = useState('')
    const [password, setpassword] = useState('')
    const [confpassword, setconfpassword] = useState('')
    const [phoneNumber, setphoneNumber] = useState('')

    const RegisterHandler = (e) => {
        e.preventDefault();
        setregisterdata({
            email,
            name,
            password,
            phoneNumber
        });
    }
    return (
        <form className="m-3" >
            <div className="form-row">
                <div className="form-group col-md-6">
                    <label>Name</label>
                    <input onChange={(e) => setname(e.target.value)} value={name} type="text" className="form-control" placeholder="Name" />
                </div>
                <div className="form-group col-md-6">
                    <label >Email</label>
                    <input onChange={(e) => setemail(e.target.value)} value={email} type="email" className="form-control" placeholder="Email" />
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-md-6">
                    <label >Password</label>
                    <input onChange={(e) => setpassword(e.target.value)} value={password} type="password" className="form-control" id="inputEmail4" placeholder="Password" />
                </div>
                <div className="form-group col-md-6">
                    <label>Confrim Password</label>
                    <input onChange={(e) => setconfpassword(e.target.value)} value={confpassword} type="password" className="form-control" placeholder="Confrim Password" />
                </div>
            </div>
            <div className="form-group">
                <label>Phone Number</label>
                <input onChange={(e) => setphoneNumber(e.target.value)} value={phoneNumber} type="text" className="form-control" placeholder="Phone Number" />
            </div>
            <div className="form-group">
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" id="gridCheck" />
                    <label className="form-check-label">
                        Check me out
      </label>
                </div>
            </div>
            <button onClick={RegisterHandler} type="submit" className={`btn ${styles.btnprimary}`}>Sign in</button>
        </form>
    )
}

const mapStateToProps = (state) => {
    return {
        registerdata: state.auth.data,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setregisterdata: data => (dispatch(actions.register(data))),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register)
