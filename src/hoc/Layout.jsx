import React from 'react';
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import 'bootstrap/dist/css/bootstrap.min.css';
function Layout(props) {
    return (
        <div>
            <Header />
            {props.children}
            <Footer />
        </div>
    );
}

export default Layout;