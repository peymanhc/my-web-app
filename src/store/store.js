import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers
} from 'redux';

import thunk from 'redux-thunk';

// Reducers
import cartReducer from './reducers/cart';
import authReducer from './reducers/auth';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    cart: cartReducer,
    auth: authReducer,
});

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk)),
);

export default store;