import * as actionTypes from '../actions/actionTypes';

const initialState = {
    data: {},
    token: localStorage.getItem('token'),
    refreshToken: localStorage.getItem('refreshToken'),
    error: null,
    loading: false,
};


const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SEND_LOGIN_INFO:
            return {
                ...state,
                data: action.data,
            }
        case actionTypes.AUTH_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                email: action.email,
            };
        case actionTypes.AUTH_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case actionTypes.SET_TOKEN:
            return {
                ...state,
                token: action.token,
            };
        case actionTypes.AUTH_LOGOUT:
            return {
                ...state,
                token: null,
                refreshToken: null
            };
        default:
            return state;
    };
};

export default authReducer;