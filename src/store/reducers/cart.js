import * as actionTypes from '../actions/actionTypes';

const initialState = {
    orders: 0,
    counter: 0,
    data: null,
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_TO_CART:
            return {
                ...state,
                orders: action.orders
            };
        case actionTypes.INC_COUNTER:
            return {
                ...state,
                counter: action.counter + 1
            }
        case actionTypes.DEC_COUNTER:
            return {
                ...state,
                counter: action.counter - 1
            }
        case actionTypes.FETCHDATA:
            return {
                ...state,
                data: action.data,
            }
        default:
            return state;
    };
};

export default cartReducer;