import * as actionTypes from './actionTypes';
import axios from '../../Data Shared/axios';

const authSuccess = (data) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        loading: false,
        data
    };
};

const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const setToken = (token) => {
    return {
        type: actionTypes.SET_TOKEN,
        token: token
    };
};

const setLoginData = (data) => ({
    type: actionTypes.SEND_LOGIN_INFO,
    data,
});

export const setLoginInfo = (data) => {
    return dispatch => {
        axios
            .post('/profile', data)
            .then(response => {
                dispatch(setLoginData(response.data));
                localStorage.setItem("email", response.data.email);
                console.log("وارد شدید");
            }).catch(error => {
                dispatch(authFail(error));
            })
    };
};

export const register = (data) => {
    return dispatch => {
        axios
            .post("/profile", data)
            .then((response) => {
                dispatch(authSuccess(response.data));
                console.log("ثبت نام با موفقیت انجام شد");
            })
            .catch((error) => {
                dispatch(authFail(error));
                console.log("Error");
            });
    };
};


export const logout = () => {
    localStorage.removeItem("email");
    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    window.location.reload();
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};


