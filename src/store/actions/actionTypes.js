export const ADD_TO_CART = 'ADD_TO_CART';
export const INC_COUNTER = 'INC_COUNTER';
export const DEC_COUNTER = 'DEC_COUNTER';
export const FETCHDATA = 'FETCHDATA';

export const SEND_LOGIN_INFO = 'SEND_LOGIN_INFO';
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const SET_TOKEN = "SET_TOKEN";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
