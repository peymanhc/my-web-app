import * as actionTypes from './actionTypes';
import axios from '../../Data Shared/axios';


const fetchdata = (data) => ({
    type: actionTypes.FETCHDATA,
    data: data
});
export const SetFetchDatas = () => {
    return dispatch => {
        axios
            .get('/posts', )
            .then(response => {
                dispatch(fetchdata(response.data))
            }).catch(err => {
                console.log("Error")
            })
    };
};


const addOrders = (orders) => {
    return {
        type: actionTypes.ADD_TO_CART,
        orders: orders,
    };
};
export const addToCart = (orders) => {
    return dispatch => {
        dispatch(addOrders(orders));
    };
};



const increment = (counter) => {
    return {
        type: actionTypes.INC_COUNTER,
        counter: counter,
    };
};

export const incrementbtn = (counter) => {
    return dispatch => {
        dispatch(increment(counter));
    };
};

const decrement = (counter) => {
    return {
        type: actionTypes.DEC_COUNTER,
        counter: counter,
    };
};

export const decrementbtn = (counter) => {
    return dispatch => {
        dispatch(decrement(counter));
    };
};
