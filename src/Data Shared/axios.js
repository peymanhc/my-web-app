import axios from 'axios';
// redux
import store from "../store/store";

const instance = axios.create({
    baseURL: 'http://localhost:3000/'
});

instance.interceptors.request.use((request) => {
    const token = store.getState().auth.token;
    if (token) {
        request.headers.Authorization = "Bearer " + token;
    }
    return request;
});


export default instance;
