import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
// redux
import { Provider } from 'react-redux';
import store from './store/store';

import Home from './pages/Home/Home';
import Layout from './hoc/Layout';
import Detail from './pages/Detail/Detail';
import Login_Register from './pages/Login-Register/Login_Register';
import Notfound from './pages/NotFound/Notfound';
import Cart from './pages/Cart/Cart';
import AboutUs from './pages/AboutUs/AboutUs';
import MyProfile from './pages/MyProfile/MyProfile';

function Routes(props) {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Layout>
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route path="/Detail" component={Detail} />
                        <Route path="/MyCart" component={Cart} />
                        <Route path="/About" component={AboutUs} />
                        <Route path="/Login_Register" component={Login_Register} />
                        <Route path="/MyProfile" component={MyProfile} />
                        <Route path='/404' component={Notfound} />
                        <Redirect from='*' to='/404' />
                    </Switch>
                </Layout>
            </BrowserRouter>
        </Provider>
    );
}

export default Routes;