import React from 'react';
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import './Cart.css'
import CartImg from '../../components/CartImg/CartImg';
import CartName from '../../components/CartName/CartName';
import CartBasket from '../../components/CartBasket/CartBasket';
import CartPrice from '../../components/CartPrice/CartPrice';
import TotalPrice from '../../components/TotalPrice/TotalPrice';
import Aux from '../../hoc/Aux';

function Cart(props) {
    return (
        <Aux className="container-fluid mt-5">
            <h2 className="mt-5 mb-5 text-center">Shopping Cart</h2>
            <div className="row justify-content-center">
                <div className="col-md-8 displaycart">
                    <div className="table-responsive">
                        <table id="myTable" className="table">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th className="text-right"><span id="amount" className="amount">Amount</span> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <CartImg />
                                    <CartName />
                                    <CartBasket />
                                    <CartPrice 
                                    price={props.orders} />
                                    <td align="right">$ <span id="amount" className="amount">{props.orders}</span></td>
                                </tr>
                            </tbody>
                            <TotalPrice price={props.orders} />
                        </table>
                    </div>
                </div>
            </div>
        </Aux>
    );
}
const mapStateToProps = state => {
    return {
        orders: state.cart.orders,
    };
};

const mapDispatchToProps = dispatch => {

    return {
        addToCart: (orders) => dispatch(actions.addToCart(orders)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);