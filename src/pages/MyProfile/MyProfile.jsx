import React from 'react';
import './MyProfile.css'
function MyProfile(props) {

    

    return (
        <div className="row w-100 m-0 p-0" >
            <div className="col-md-3 m-0 p-0" >
                <div className="leftProfile">
                    <img alt="Profile" className="imgProfile" src={require("../../Resources/images/profile_image.jpg")} />
                    <h1 className="mt-4 text-white font-weight-bold" >Peymanhc</h1>
                    <h5 className="text-white" >Name: Peyman Hadavi</h5>
                    <p className="text-white small mt-4 mb-0" >Email: Peymanhc@gmail.com</p>
                    <small className="text-white" >Phone Number: +989030525589</small>
                </div>
            </div>
            <div className="col-md-9 m-0 p-0">
                <form className="form-card">
                    <fieldset className="form-fieldset">
                        <legend className="form-legend">Your Profile</legend>
                        <div className="form-radio form-radio-inline">
                            <div className="form-radio-legend mb-3">How much do you like pomegranates?</div>
                            <label className="form-radio-label">
                                <input className="form-radio-field" type="radio"/>
                                <i className="form-radio-button"></i>
                                <span>afew</span>
                            </label>
                            <label className="form-radio-label">
                                <input className="form-radio-field" type="radio"/>
                                <i className="form-radio-button"></i>
                                <span>medium</span>
                            </label>
                            <label className="form-radio-label">
                                <input className="form-radio-field" type="radio" />
                                <i className="form-radio-button"></i>
                                <span>Much</span>
                            </label>
                            <small className="form-element-hint">Feel free to choose</small>
                        </div>
                        <div className="form-element form-input">
                            <input className="form-element-field" placeholder=" " type="text" />
                            <div className="form-element-bar"></div>
                            <label className="form-element-label">UserName</label>
                        </div>
                        <div className="form-element form-input">
                            <input className="form-element-field" placeholder=" " type="text" />
                            <div className="form-element-bar"></div>
                            <label className="form-element-label">Name</label>
                        </div>
                        <div className="form-element form-input">
                            <input className="form-element-field" placeholder=" " type="text" />
                            <div className="form-element-bar"></div>
                            <label className="form-element-label">Last Name</label>
                        </div>
                        <div className="form-element form-input">
                            <input className="form-element-field" placeholder=" " type="email" />
                            <div className="form-element-bar"></div>
                            <label className="form-element-label">Email</label>
                        </div>
                        <div className="form-element form-input">
                            <input className="form-element-field" placeholder=" " type="email" />
                            <div className="form-element-bar"></div>
                            <label className="form-element-label">Phone Number</label>
                        </div>
                        <div className="form-checkbox form-checkbox-inline">
                            <div className="form-checkbox-legend mb-3 ">What fruit do you like?</div>
                            <label className="form-checkbox-label">
                                <input className="form-checkbox-field" type="checkbox" />
                                <i className="form-checkbox-button"></i>
                                <span>Pomegranate</span>
                            </label>
                            <label className="form-checkbox-label">
                                <input className="form-checkbox-field" type="checkbox" />
                                <i className="form-checkbox-button"></i>
                                <span>Apple</span>
                            </label>
                            <label className="form-checkbox-label">
                                <input className="form-checkbox-field" type="checkbox" />
                                <i className="form-checkbox-button"></i>
                                <span>Cherries</span>
                            </label>
                            <label className="form-checkbox-label">
                                <input className="form-checkbox-field" type="checkbox" />
                                <i className="form-checkbox-button"></i>
                                <span>Orange</span>
                            </label>
                            <label className="form-checkbox-label">
                                <input className="form-checkbox-field" type="checkbox" />
                                <i className="form-checkbox-button"></i>
                                <span>Pineapple</span>
                            </label>
                        </div>
                        <div className="form-element form-textarea">
                            <textarea className="form-element-field"></textarea>
                            <div className="form-element-bar"></div>
                            <label className="form-element-label">Your Address</label>
                        </div>
                    </fieldset>
                    <div className="form-actions">
                        <button className="form-btn" type="submit">Send inquiry</button>
                        <button className="form-btn-cancel -nooutline" type="reset">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default MyProfile;