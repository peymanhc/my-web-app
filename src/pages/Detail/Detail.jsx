import React from 'react';
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import DetailCart from '../../components/DetailCart/DetailCart';
import AddBuy from '../../components/AddBuy/Addbuy'
import DetailNav from '../../components/DetailNav/DetailNav';
import Specifications from '../../components/DetailSpecification/Specifications';
import UserComments from '../../components/DetailUserComments/UserComments';
import Aux from '../../hoc/Aux'


function Detail(props) {
    return (
        <Aux>
            <DetailNav />
            <div className="tab-content" id="nav-tabContent">
                <div
                    className="tab-pane fade show active"
                    id="nav-home" role="tabpanel"
                    aria-labelledby="nav-home-tab">
                    <DetailCart />
                </div>
                <div
                    className="tab-pane fade"
                    id="nav-profile"
                    role="tabpanel"
                    aria-labelledby="nav-profile-tab">
                    <Specifications />
                </div>
                <div
                    className="tab-pane fade"
                    id="nav-contact"
                    role="tabpanel"
                    aria-labelledby="nav-contact-tab">
                    <UserComments />
                </div>
            </div>
            <AddBuy
                price={props.orders}
            />
        </Aux>
    );
}

const mapStateToProps = state => {
    return {
        orders: state.cart.orders,
    };
};

const mapDispatchToProps = dispatch => {

    return {
        addToCart: (orders) => dispatch(actions.addToCart(orders)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);