import React from 'react';
import styels from './LoginRegister.module.css'
import Login from '../../components/Login/Login';
import Register from '../../components/Register/Register';
import Aux from '../../hoc/Aux';

function Login_Register(props) {
    return (
        <Aux>
            <div className={styels.box}>
                <div className={styels.box1}>
                    <h2 className="text-center mt-5" >Register</h2>
                    <Register />
                </div>

                <div className={styels.box2}>
                <h2 className="text-center mt-5" >Login</h2>
                    <Login />
                </div>
            </div>
        </Aux>
    );
}

export default Login_Register;