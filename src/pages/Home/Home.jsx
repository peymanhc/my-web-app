import React, { useEffect } from 'react';
// redux
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import Card from '../../components/Card/card';
import './Home.css'
import Addbuy from '../../components/AddBuy/Addbuy';
import Contact from '../../components/Contact/Contact';
import Pagination from '../../components/Pagination/Pagination';
import Category from '../../components/Category/Category';
import Aux from '../../hoc/Aux'
import Search from '../../components/Search/Search';

function Home({ orders, count, SetFetchdata, counteachCard }) {

    useEffect(() => {
        SetFetchdata()
    }, [])

    return (
        <Aux>
            <div className="box">
                <div className="box-1 ">
                    <div className="row mb-5 " >
                        <div className="col-md-12 " >
                            <Search />
                            <Category />
                        </div>
                    </div>
                    <div className="row" >
                        {counteachCard === null
                            ? <h5 className="text-center w-100" >Datas Not load<br /><small>If you are Testing please Run Server </small></h5>
                            : counteachCard.map((item, i) => (
                                <Card
                                    key={item.id}
                                    title={item.title}
                                    price={item.price}
                                    description={item.description}
                                    image={item.url} />
                            ))}
                    </div>
                    <Pagination />
                </div>
                <div className="box-2">
                    <Addbuy
                        counts={count}
                        price={orders} />
                    <Contact />
                </div>
            </div>
        </Aux>
    );
};


const mapStateToProps = state => {
    return {
        orders: state.cart.orders,
        count: state.cart.counter,
        counteachCard: state.cart.data
    };
};

const mapDispatchToProps = dispatch => {

    return {
        addToCart: (orders) => dispatch(actions.addToCart(orders)),
        incrementbtn: (count) => dispatch(actions.incrementbtn(count)),
        SetFetchdata: data => dispatch(actions.SetFetchDatas(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);