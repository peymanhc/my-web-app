import React from 'react'
import './About.css'
import LeftAbout from '../../components/LeftAbout/LeftAbout'
import RightAbout from '../../components/RightAbout/RightAbout'
import Aux from '../../hoc/Aux'
function AboutUs() {
    return (
        <Aux>
            <div className="aboutus-section">
                <div className="container">
                    <div className="row">
                        <LeftAbout />
                        <div className="col-md-3 col-sm-6 col-xs-12">
                        </div>
                        <RightAbout />
                    </div>
                </div>
            </div>
        </Aux>
    )
}

export default AboutUs
