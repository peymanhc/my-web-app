import React from 'react'
import styles from './NotFound.module.css'

const Notfound = () => {
    return (
        <div className={styles.notfoundp}>
            <div className={styles.notfound}>
                <div className={styles.notfound404}>
                    <h1>404</h1>
                    <h2>Page not found</h2>
                </div>
                <a href="#Report">Report</a>
                <a href="/">Homepage</a>

            </div>
        </div>
    )
}

export default Notfound
